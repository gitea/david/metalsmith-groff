# metalsmith-groff

This plugin for [Metalsmith][] is aimed at generating documents written for **[groff][]**. It has very few Javascript dependencies, but expects **groff** to be correctly installed on the system.

[![NPM version](https://img.shields.io/npm/v/metalsmith-groff.svg?style=flat-square)](https://www.npmjs.com/package/metalsmith-groff)

[metalsmith]: http://metalsmith.io
[groff]: https://www.gnu.org/software/groff

##  Usage

If using the CLI for Metalsmith, metalsmith-groff can be used like any other plugin by including it in `metalsmith.json`.

```json
{
  "plugins": {
    "metalsmith-groff": {}
  }
}
```

In Metalscript's JavaScript API, metalsmith-groff can be used like any other plugin by attaching it to the function invocation chain on the Metalscript object.

```js
const groff = require('metalsmith-groff')

require('metalsmith')(__dirname)
  .use(groff())
  .build()
```

If metalsmith-groff is used as above without any argument, it will handle any file ending with `.ms`, `.me`, `.mm` or `.mom` and call **groff** with the eponymous macro to build a PDF document. The original file is dropped and replaced by the produced PDF one. To extend this default behaviour, it is possible to provide options as a simple objet.

## Options

Here are the options handled by the plugin. A good share of them is used to configure the call to **groff**, so reading **groff** manual will provide additional and useful information.

- `device` is the **groff** output device to be used. Default is `pdf`, and other common devices provided with **groff** are `html` or `ps`.

- `encoding` sets the input encoding. It is the `-K` option of **groff** and defaults to `utf8`.

- `macroPackages` is an array of macros to use. By default, the plugin tries to choose a classical macro package using the file extension.

- `macroPaths` is an array of additional paths to search for macros. The default value is empty, see `man groff_tmac` for further explanation about the search sequence.

- `preprocessors` is an array of preprocessors to be used by **groff**. Common preprocessors are `tbl`, `pic` or `soelim`. It is empty by default.

- `pattern` is the matcher used to determine the files to be transformed. It is used with **[minimatch][]**. The default value (`**/*.@(me|mm|ms|mom)`) matches all files ending with `.ms`, `.me`, `.mm` or `.mom` at any depth.

- The source file is dropped if `source` is false (the default behaviour), but remains in the tree along the translated document if true.

[minimatch]: https://www.npmjs.com/package/minimatch
