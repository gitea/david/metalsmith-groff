/*
 * A simple plugin for Metalsmith to transform Groff documents.
 * Copyright (C) 2022  David Soulayrol <david@soulayrol.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const debug = require('debug')('metalsmith-groff')
const minimatch = require('minimatch')
const path = require('path')
const spawn = require('child_process').spawn

/**
 * Expose `plugin`.
 */
module.exports = plugin

/**
 * Metalsmith plugin to convert groff documents.
 *
 * @return {Function}
 */
function plugin (options) {

  const defaults = {
    device: 'pdf',
    encoding: 'utf8',
    exe: 'groff',
    macroPackages: [],
    macroPaths: [],
    preprocessors: [],
    pattern: '**/*.@(me|mm|ms|mom)',
    source: false
  }

  let pluginOptions = defaults

  /* Merge given options with defaults. */
  if (options) {
    for (var property in options) {
      pluginOptions[property] = options[property];
    }
  }

  var configureMacroPackages = function (userMacros, filename) {
    const classical = ['man', 'mandoc', 'mdoc', 'me', 'mm', 'mom', 'ms']

    let arguments = [],
        defaultMacro = path.extname(filename).replace('.', '')

    if (userMacros.length === 0) {
      if (classical.includes(defaultMacro)) {
        debug('Selected macro package ' + defaultMacro)
        arguments.push('-' + defaultMacro)
      }
    } else {
      userMacros.forEach(macro => {
        arguments.push('-m')
        arguments.push(macro)
      })
    }

    return arguments
  }

  var configureMacroPaths = function (paths) {
    let arguments = []

    paths.forEach(p => {
      arguments.push('-M')
      arguments.push(p)
    })

    return arguments
  }

  var configurePreprocessors = function (preprocessors) {
    const mapping = {
      chem: '-j',
      eqn: '-e',
      grn: '-g',
      pic: '-p',
      preconv: '-k',
      refer: '-R',
      soelim: '-s',
      tbl: '-t'
    }

    let arguments = []

    preprocessors.forEach(preprocessor => {
      let p = mapping[preprocessor]

      if (p === undefined)
        debug('Ignoring undefined preprocessor ' + preprocessor)
      else
        arguments.push(p)
    })

    return arguments
  }

  var buildArguments = function (filename) {
    let arguments = []

    arguments.push(configurePreprocessors(pluginOptions.preprocessors))
    arguments.push(configureMacroPaths(pluginOptions.macroPaths))
    arguments.push(configureMacroPackages(pluginOptions.macroPackages, filename))

    arguments.push(['-K', pluginOptions.encoding])

    arguments.push(['-T', pluginOptions.device])

    return arguments.flat()
  }

  var groff = function(filename, data, output, resolve, reject) {
    let arguments = buildArguments(filename), buffer, groff

    debug('Spawning groff ' + arguments.join(' '))
    groff = spawn(pluginOptions.exe, arguments)

    groff.stdout.on('data', function (data) {
      if (buffer === undefined)
        buffer = Buffer.from(data)
      else
        buffer = Buffer.concat([buffer, data])
    })

    groff.stdout.on('close', function (data) {
      output.contents = buffer
      resolve()
    })

    groff.stderr.on('data', function (date) {
      debug(data)
    })

    groff.on('exit', function (code) {
      if (code != 0) {
        debug('Failed: ' + code)
        reject('Failed: ' + code)
      }
    })

    groff.on('close', resolve)

    groff.stdin.write(data.toString())
    groff.stdin.end()
  }

  return function convert (files, metalsmith) {
    let documents = Object.keys(files).filter(minimatch.filter(pluginOptions.pattern))

    return Promise.all(documents.map(file => {
      return new Promise((resolve, reject) => {
        let dir = path.dirname(file),
            destination = path.basename(file, path.extname(file)) + '.' + pluginOptions.device,
            data = {
              contents: Buffer.from(files[file].contents),
              mode: files[file].mode
            }
            input = data.contents

        if (dir !== '.')
          destination = path.join(dir, destination)

        debug('Converting file: %s', file)

        if (!pluginOptions.source)
          delete files[file]
        files[destination] = data

        groff(file, input, data, resolve, reject)
      })
    }))
  }
}
