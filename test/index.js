/*
 * A simple plugin for Metalsmith to transform Groff documents.
 * Copyright (C) 2022  David Soulayrol <david@soulayrol.name>
 *
 * This file is part of metalsmith-groff distribution.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Metalsmith = require('metalsmith')
const assert = require('assert')
const debug = require('debug')
const groff = require('..')
const path = require('path')

const { it, describe } = require('mocha')

function run (done, fixture, options, checker) {
  const metalsmith = Metalsmith(path.join('test/fixtures/', fixture)),
        logger = debug('metalsmith-groff:test:' + fixture)

  options.exe = 'test/groff_mock'

  metalsmith
    .use(groff(options))
    .use((files, metalsmith) => {
      Object.keys(files).forEach(filename => {
        logger('Checking ' + filename + ": " + files[filename].contents.toString('utf8'))
        checker(filename, files[filename], logger)
      })
    })
    .process((err) => {
      if (err)
        throw err
      done()
    })
}

describe('metalsmith-groff', function () {

  it('should determine automatically the macro from the file extension', function (done) {

    run(done, 'noconfig', {}, function (filename, file, log) {
      let arguments = file.contents.toString('utf8').split(' '),
          expectedMacro = '-' + path.basename(filename, '.pdf').split('_')[1]

      assert.notEqual(arguments.find(item => item === expectedMacro), undefined,
                      'Expected macro ' + expectedMacro + ' not used!')
    })
  })

  it('should use the given macros and respect the order', function (done) {
    let macros = ['my_macro', 'ms']

    run(done, 'macros', {
        macroPackages: macros
    }, function (filename, file, log) {
      let arguments = file.contents.toString('utf8').split(' ')

      while (arguments.length > 0) {
        let item = arguments.shift()

        if (item === '-m') {
          assert.strictEqual(arguments.shift(), macros.shift(), 'Unexpected macro!')
        }
      }
      assert.equal(macros.length, 0, 'Macros mismatch!')
    })
  })

  it('should use the given paths for the macros', function (done) {
    let paths = ['/tmp', './here', '../another/path']

    run(done, 'macros', {
      macroPaths: paths
    }, function (filename, file, log) {
      let arguments = file.contents.toString('utf8').split(' ')

      while (arguments.length > 0) {
        let item = arguments.shift()

        if (item === '-M') {
          /* Order is not important. */
          let p = arguments.shift(),
              i = paths.indexOf(p)
          assert.notEqual(i, -1, 'Found unknown path!')
          paths.splice(i, 1)
        }
      }
      assert.equal(paths.length, 0, 'Paths mismatch!')
    })
  })

  it('should use the given preprocessors and respect the order', function (done) {
    let preprocessors = {
      '-e': 'eqn',
      '-g': 'grn',
      '-j': 'chem',
      '-k': 'preconv',
      '-p': 'pic',
      '-R': 'refer',
      '-s': 'soelim',
      '-t': 'tbl'
    }

    run(done, 'macros', {
      preprocessors: Object.values(preprocessors)
    }, function (filename, file, log) {
      let arguments = file.contents.toString('utf8').split(' ')

      while (arguments.length > 0) {
        let item = arguments.shift()

        if (preprocessors[item] !== undefined)
          delete preprocessors[item]
      }

      assert.strictEqual(Object.keys(preprocessors).length, 0,
                         'Preprocessors are missing: ' + Object.values(preprocessors))
    })
  })

  it('should keep the source file if requested to do so', function (done) {
    const metalsmith = Metalsmith('test/fixtures/source'),
          logger = debug('metalsmith-groff:test:source')

    metalsmith
      .use(groff({
        exe: 'test/groff_mock',
        source: true
      }))
      .use((files, metalsmith) => {
        assert.equal(Object.keys(files).length, 2, 'Expected two files on output!')
        assert.equal(files['foo.ms'].contents.toString('utf8'), 'foo',
                     'Source file was modified!')
      })
      .process((err) => {
        if (err)
          throw err
        done()
      })
  })
})
